//REGION 1
var gBASE_URL = "https://62454a477701ec8f724fb923.mockapi.io/api/v1/";
var gStudent = [];
var gSubject = [];
var gId = 0;
var gGrade_Obj = 0;
var gTable_User = $("#table-user").DataTable({
    columns: [
        { data: "id" },
        { data: "studentId" },
        { data: "subjectId" },
        { data: "grade" },
        { data: "examDate" },
        { data: "action" }
    ],
    columnDefs: [
        {
            targets: 0,
            render: function (data, type, row, meta) {
                // Tính toán số thứ tự tăng dần
                var index = meta.row + 1;
                return index;
            }
        },
        {
            targets: 1,
            render: studentIdToStudent
        },
        {
            targets: 2,
            render: subjectIdToSubject
        },
        {
            targets: 5,
            defaultContent: `
            <button class="btn btn-info btn-edit"  style="margin-right: 10px;"><i class="fas fa-pen"></i> Sửa</button>
            <button class="btn btn-danger btn-delete"><i class="fas fa-trash"></i> Xóa</button>`
        }
    ]
})
//REGION 2
onPageLoading()
//gán sự kiện nút filter
$("#btn-filter").on('click', function () {
    onBtnFilterClick();
})
//gán sụ kiện nút add 
$("#btn-add").on('click', function(){
    onBtnAddClick();
})
//gán sự kiện nút add trên modal
$("#modal-btn-add").on('click', function(){
    onBtnAddModalClick();
})
//gán sự kiện nút sửa
$("#table-user").on('click', '.btn-edit', function(){
    onBtnEditClick(this);
})
//gán sự kiện nút save trên modal edit
$("#btn-edit-save").on('click', function(){
    onBtnSaveClick();
})
//gán sự kiện nút delete trên modal
$("#table-user").on('click', '.btn-delete', function(){
    onBtnDeleteClick(this);
});
//gán sự kiện nút xác nhận delete trên modal
$("#modal-btn-delete").on('click', function(){
    onBtnConfirmDeleteClick();
})
//REGION 3 
function onPageLoading() {
    getStudentList();
    getSubjectList();
    getGradesList();
}
//hàm sử lý sự kiện nút btn click
function onBtnFilterClick() {
    var vFilterObj = {
        student: "",
        subject: "",
    }
    vFilterObj.student = $("#select-student").val();
    vFilterObj.subject = $("#select-subject").val();
    filterOrder(vFilterObj);
}
//hàm sử lý sự kiện nút btn add click
function onBtnAddClick(){
    $("#modal-add").modal('show');
}
//hàm thực hiện sự kiện nút add trên modal
function onBtnAddModalClick(){
    //b1 chuản bị dữ liệu, thu thập dữ liệu
    var vAddObj = {
        student: "",
        subject: "",
        grades: "",
        ngayThi: "",
    }
    getAddObj(vAddObj);
    var vIsCheck = validateObj(vAddObj)
    if(vIsCheck){
        //gọi api để thêm
        createGradeClick(vAddObj);
        //xử lý hiển thị
        displayProcessing();
    }

}
//hàm sử lý sự kiện nút edit click
function onBtnEditClick(paramBtn){
    gId = getId(paramBtn);
    //gọi api lấy obj thông qua id
    getObjById(gId);
    $("#modal-edit").modal('show');
}
//hàm thục hiện sử lý sự kiện nút seve trên modal
function onBtnSaveClick(){
    var vGradesObj = {
        student: "",
        subject: "",
        grade: "",
        ngayThai: "",
    };
    getDataModaledit(vGradesObj);
    $.ajax({
        url: gBASE_URL + "/grades",
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(vGradesObj),
        success: function(response){
            console.log(response);
            notificationUpdateGrade();
        },
        error: function(err){
            console.log(err);
        }
    })
}
//hàm sử lý sự kiện nút delete được click
function onBtnDeleteClick(paramBtn){
    getDataBtnDeleteTable(paramBtn);
    //gọi api lấy obj thông qua id
    getObjById();
    $("#modal-delete").modal("show");
}
//hàm sử lý sự kiện nút confirm delete được click
function onBtnConfirmDeleteClick(){
    deleteGrade();
}
//REGION 4
//hàm gọi api để lấy danh sách học sinh
function getStudentList() {
    $.ajax({
        url: gBASE_URL + "/students",
        type: "GET",
        success: function (res) {
            loadStudentToSelect(res);
            gStudent = res;
            console.log(gStudent);
        },
        error: function (err) {
            console.log(err.status);
        }
    })
}
//hàm gọi api để lấy danh sách môn học
function getSubjectList() {
    $.ajax({
        url: gBASE_URL + "/subjects",
        type: "GET",
        success: function (res) {
            loadSubjectToSelect(res);
            gSubject = res;
            console.log(gSubject);
        },
        error: function (err) {
            console.log(err.status);
        }
    })
}
//hàm gọi api để lấy danh sách điểm và ngày thi
function getGradesList() {
    $.ajax({
        url: gBASE_URL + "/grades",
        type: "GET",
        success: function (res) {
            console.log(res);
            loadDataToTable(res);
        },
        error: function (err) {
            console.log(err.status);
        }
    })
}
//hàm truyền dữ liệu vào select student
function loadStudentToSelect(paramRes) {
    for (let bI = 0; bI < paramRes.length; bI++) {
        $("#select-student").append($("<option>", {
            value: paramRes[bI].id,
            text: paramRes[bI].firstname + paramRes[bI].lastname
        }))
    }
}
//hàm truyền dữ liệu vào select subject
function loadSubjectToSelect(paramRes) {
    for (let bI = 0; bI < paramRes.length; bI++) {
        $("#select-subject").append($("<option>", {
            value: paramRes[bI].id,
            text: paramRes[bI].subjectName,
        }))
    }
}
//hàm load dữ liệu Grades to table
function loadDataToTable(paramRes) {
    gTable_User.clear();
    gTable_User.rows.add(paramRes);
    gTable_User.draw();
}
function studentIdToStudent(paramData) {
    var vStudent = "";
    for (var bI = 0; bI < gStudent.length; bI++) {
        if (paramData == gStudent[bI].id) {
            vStudent = (gStudent[bI].firstname + " " + gStudent[bI].lastname);
        }
    }

    return vStudent;
}
function subjectIdToSubject(paramData) {
    var vSubject = "";
    for (var bI = 0; bI < gSubject.length; bI++) {
        if (paramData == gSubject[bI].id) {
            vSubject = (gSubject[bI].subjectName);
        }
    }
    return vSubject
}
//hàm tiến hành lọc dữ liêu
function filterOrder(paramFilter) {
    var vStudent = paramFilter.student;
    var vSubject = paramFilter.subject;
    //lọc và tìm kiếm dữ liệu trong DataTable
    gTable_User.search(vStudent + " " + vSubject).draw();
}
//hàm thu thập dữ liệu để thêm đối tượng
function getAddObj(paramObj){
    paramObj.student = $("#inp-student").val().trim();
    paramObj.subject = $("#inp-subject").val().trim();
    paramObj.grade = $("#inp-grade").val().trim();
    paramObj.ngayThi = $("#inp-ngay-thi").val().trim();
}
//hàm kiểm tra dữ liệu thêm đối tượng
function validateObj(paramObj){
    if(paramObj.student == ""){
        alert("Bạn chưa nhập tên sinh viên");
        $("#inp-student").focus();
        return false;
    }
    if(paramObj.subject == ""){
        alert("Bạn chưa nhập tên môn học");
        $("#inp-subject").focus();
        return false;
    }
    if(paramObj.grade == ""){
        alert("Bạn chưa nhập điểm");
        $("#inp-grade").focus();
        return false;
    }
    if(paramObj.ngayThi == ""){
        alert("Bạn chưa nhập ngày thi");
        $("#inp-ngay-thi").focus();
        return false;
    }
    return true;
}
//hàm gọi api để thêm 
function createGradeClick(paramObj){
    $.ajax({
        url: gBASE_URL + "/grades",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(paramObj),
        success: function(response){
            console.log(response);
        },
        error: function(err){
            console.log(err.status);
        }
    })
}
//hàm sử lý hiển thị
function displayProcessing(){
    alert("Đã thêm thành công");
    onPageLoading();
    $("#modal-add").modal('hide');
}
//hàm lấy id khi nút sửa được click
function getId(paramBtn){
    var vTableRow = $(paramBtn).parents('tr');
    var vUserRowData = gTable_User.row(vTableRow).data();
    return vUserRowData.id;
}
//hàm lấy obj thông qua id
function getObjById(){
    $.ajax({
        url: "https://62454a477701ec8f724fb923.mockapi.io/api/v1/grades/" + gId,
        type: 'GET',
        success: function (paramGradeObj) {
            showObjToModal(paramGradeObj);
        },
        error: function (err) {
          console.log(err.status);
        },
      });
}
function showObjToModal(paramId) {
    $('#inp-edit-student').val(paramId.studentId);
    $('#inp-edit-subject').val(paramId.subjectId);
    $('#inp-edit-grade').val(paramId.grade);
    $('#inp-edit-ngay-thi').val(paramId.examDate);
}
//hàm thu thập dữ liệu edit trên modal
function getDataModaledit(paramGrade){
    paramGrade.student = $("#inp-edit-student").val().trim();
    paramGrade.subject = $("#inp-edit-subject").val().trim();
    paramGrade.grade = $("#inp-edit-grade").val().trim();
    paramGrade.ngayThi = $("#inp-edit-ngay-thi").val().trim();
}
//hàm hiển thị thông báo update thành công
function notificationUpdateGrade(){
    alert("Cập nhật thành công");
    onPageLoading();
    $("#modal-edit").modal('hide');
}
//hàm lấy dữ liệu khi nút delete được ấn
function getDataBtnDeleteTable(paramBtn){
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramBtn).parents("tr");
    //Lấy datatable row
    var vDatatableRow = gTable_User.row(vRowSelected);
    //Lấy data của dòng 
    var vIdData = vDatatableRow.data();
    var vIdOrder = vIdData.id;
    gId = vIdOrder;
}
//hàm gọi api để delete một đối tượng
function deleteGrade(){
    $.ajax({
        url: gBASE_URL + "/grades/" + gId,
        type: "DELETE",
        success: function(){
            notificationDelete()
        },
        error: function(err){
            console.log(err.status);
        }
    })
}
//hàm hiển thị thông báo delete thành công
function notificationDelete(){
    alert("Delete thành công");
    onPageLoading();
    $("#modal-delete").modal('hide');
}
